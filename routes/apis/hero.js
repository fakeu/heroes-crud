const express = require("express");
const router = express.Router();
const Hero = require("../../models/Hero");

router.get("/", (req, res, next) => {
  Hero.find()
    .then(heroes => {
      res.json(heroes);
    })
    .catch(err => {
      res.send(err.message);
    });
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;

  Hero.findById(id)
    .then(hero => {
      res.json(hero);
    })
    .catch(err => {
      res.send(err.message);
    });
});

router.post("/", (req, res, next) => {
  const { name, health, attack, defense, source } = req.body;
  const newHero = new Hero({
    name,
    health,
    attack,
    defense,
    source
  });

  newHero
    .save()
    .then(hero => {
      res.json(hero);
    })
    .catch(err => {
      res.send(err.message);
    });
});

router.put("/:id", (req, res, next) => {
  const id = req.params.id;
  const { name, health, attack, defense, source } = req.body;

  Hero.findById(id)
    .then(hero => {
      hero.name = name || hero.name;
      hero.health = health || hero.health;
      hero.attack = attack || hero.attack;
      hero.defense = defense || hero.defense;
      hero.source = source || hero.source;
      hero
        .save()
        .then(hero => {
          res.send({
            message: "Hero updated",
            hero
          });
        })
        .catch(err => {
          res.send(err.message);
        });
    })
    .catch(err => {
      res.send(err.message);
    });
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;

  Hero.findById(id)
    .then(hero => {
      hero
        .delete()
        .then(hero => {
          res.send({
            message: "Hero deleted"
          });
        })
        .catch(err => {
          res.send(err.message);
        });
    })
    .catch(err => {
      res.send(err.message);
    });
});

module.exports = router;

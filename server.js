const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

const db = require("./config/db").database;

mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => {
    console.log("db connected");
  })
  .catch(err => console.log(err));

const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.send("Hello World");
});

const heroRoutes = require("./routes/apis/hero");

app.use("/api/hero", heroRoutes);

app.listen(port, () => {
  console.log("server started on port " + port);
});

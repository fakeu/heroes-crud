const mongoose = require("mongoose");

const HeroShema = mongoose.Schema({
  name: {
    type: String,
    require: true
  },
  health: {
    type: String,
    require: true
  },
  attack: {
    type: String,
    require: true
  },
  defense: {
    type: String,
    require: true
  },
  source: {
    type: String,
    require: false
  }
});

module.exports = mongoose.model("Hero", HeroShema);

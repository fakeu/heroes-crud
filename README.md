# Heroes CRUD service

### App url

http://localhost:5000/

##

### Get heroes list

#### [GET] http://localhost:5000/api/hero

##

### Get hero by name

#### [GET] http://localhost:5000/api/hero/{ hero name }

_example:_ http://localhost:5000/api/hero/Ryu

##

### Add new hero

#### [POST] http://localhost:5000/api/hero/

_example(body):_

```json
{
  "name": "Balrog",
  "health": 55,
  "attack": 4,
  "defense": 6,
  "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif"
}
```

##

### Update hero's information

#### [PUT] http://localhost:5000/api/hero/{ hero id }

_example(body):_ http://localhost:5000/api/hero/5ce7fceda1257327dc527cb3

```json
{
  "health": 155,
  "attack": 14,
  "defense": 16
}
```

##

### Delete hero

#### [DELETE] http://localhost:5000/api/hero/{ hero id }

_example:_ http://localhost:5000/api/hero/5ce7fceda1257327dc527cb3

## License

MIT
